Copyright (C) 2002-2013 Python Software Foundation; All Rights Reserved

The Python Software Foundation (PSF) holds copyright on all material
in this project.  You may use it under the terms of the PSF license;
see LICENSE.txt.

This is a (kind of private) fork of the famous Spambayes 
(https://github.com/smontanaro/spambayes/). As Spambayes IMHO is still one
of the best and most versatile spam filters ever made, I keep it alive
for my purposes.

SpamBayes is a tool used to segregate unwanted mail (spam) from the mail you
want (ham).  Before SpamBayes can be your spam filter of choice you need to
train it on representative samples of email you receive.  After it's been
trained, you use SpamBayes to classify new mail according to its spamminess
and hamminess qualities.

For more details, see spambayes/README.txt.

The goal of this fork (work in progress) is to:
- remove some stuff which is obsolete (due to evolution of Python)
- use Sqlite as the one and only database backend
- migrate to Python 3.x

The only scripts/contents which are updated and kept are the ones which are 
relevant for my use case: IMAP, running on Linux
This currently includes:
- sb_imapfilter.py
- sm_dbexpimp.py
and all the SW parts used by this scripts.

All other parts of the SW are considered dead code and are not maintained
or even removed.
