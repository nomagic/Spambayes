#! /usr/bin/env python

"""sb_migrate.py - Message database migration from DBM to sqlitedict

Examples:


    Import mymsg.db into a new sqlite DB mymsg.sqlite
        sb_migrate -d mymsg.sqlite -f mymsg.db


# This module is part of the spambayes project, which is Copyright 2002-2007
# The Python Software Foundation and is covered by the Python Software
# Foundation license.
"""
from __future__ import generators

__author__ = "Tim Stone <tim@fourstonesExpressions.com>"


import sys, os, getopt, errno
from sqlitedict import SqliteDict
import shelve
import bsddb


def runImport(dbFN,  inFN):

    target = SqliteDict(dbFN, 'n')
    dbm = bsddb.hashopen(inFN, 'r')
    db = shelve.Shelf(dbm)

    for k in  db.keys():
        target[k]=db[k]
    target.commit()
    target.close()

if __name__ == '__main__':

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hd:f:')
    except getopt.error, msg:
        print >> sys.stderr, str(msg) + '\n\n' + __doc__
        sys.exit()

    dbFN = None
    flatFN = None

    for opt, arg in opts:
        if opt == '-h':
            print >> sys.stderr, __doc__
            sys.exit()
        elif opt == '-d':
            dbFN = arg
        elif opt == '-f':
            flatFN = arg
    if (dbFN and flatFN):
            runImport(dbFN, flatFN)
    else:
        print >> sys.stderr, __doc__
